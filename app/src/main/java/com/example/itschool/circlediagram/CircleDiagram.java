package com.example.itschool.circlediagram;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by IT SCHOOL on 26.11.2016.
 */
public class CircleDiagram extends View {

    private Paint Paint = new Paint();
    private static final String LOG_TAG = "Circle Diagram";
    private float[] data = new float[1];

    private int[] colors = {Color.CYAN, Color.GREEN, Color.BLUE, Color.RED, Color.YELLOW, Color.MAGENTA};

    public CircleDiagram(Context context) { //
        super(context);
    }

    public CircleDiagram(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setData (int[] array) { // принимаем входные данные
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        float persent = sum / 100.0f;
        float[] persents = new float[array.length];
        for (int i = 0; i < array.length; i++) {
            persents[i] = array[i] / persent;
            Log.d(LOG_TAG, persents[i] + "");
        }
        data = persents;

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawARGB(255, 68, 68, 68); // закрашиваем фон
        float width = canvas.getWidth(); // ширина canvas
        float height = canvas.getHeight(); // высота канвас

        // вычисляем центр canvas
        float centerX = width / 2;
        float centerY = height / 4;
        float radius = Math.min(width, height) / 4; // радиус круговой диаграмы

        RectF rect = new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
        Paint paint = new Paint();

        float currentAngle = 0;
        float percentAngle = 360.0f / 100.0f;
        int coord = (int) (width - 80); // Шаг от верха до текста
        Paint.setColor(Color.BLACK);
        canvas.drawCircle(centerX, centerY+5, radius + 2, Paint); // Тень от графика
        Log.d(LOG_TAG, "Добро пожаловать в программу!");
        for (int i = 0; i < data.length; i++) {
            paint.setColor(colors[i]);
            Log.d(LOG_TAG,"Производство " + (i+1) + " Угол: " + currentAngle + " Проценты: " + data[i]);
            float angle = data[i] * percentAngle;
            canvas.drawArc(rect, currentAngle, angle, true, paint);
            currentAngle += angle;
            Paint.setColor(Color.BLACK);
            canvas.drawRect(9, coord+2, 31, coord+22, Paint); //Тень от квадратов
            Paint.setColor(colors[i]);
            canvas.drawRect(10, coord, 30, coord+20, Paint);
            Paint.setTextSize(32);
            Paint.setColor(Color.WHITE);
            canvas.drawText("Производство "+ (i+1) + " : " + data[i] + " %", 40, coord+20 , Paint);
            coord = coord + 40; // Шаг между строками


        }
        Paint.setColor(Color.BLACK);
        canvas.drawCircle(centerX, centerY-5, radius-80, Paint); // Тень от графика внутри
        Paint.setColor(Color.DKGRAY);
        canvas.drawCircle(centerX, centerY, radius-80, Paint);

    }
}
